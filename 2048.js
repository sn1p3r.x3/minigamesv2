
const gridDisplay = document.querySelector('.grid');
	const scoreDisplay= document.getElementById('score')
	const resultDisplay=document.getElementById('result')
	const width=4;
	let squares=[];
	let score=0;




	function createboard() {
		for (var i = 0; i <width*width; i++) {
			let square=document.createElement('div');
			square.innerHTML=0;
			gridDisplay.appendChild(square)
			squares.push(square);
		}
		generate()
		generate()
		

	} 
	createboard();

	/*function PopUp()
	{
		document.getElementById("popup-1").classList.toggle('active');
	};*/

	//generating a number
	function generate() {
		 var randomnumber=Math.floor(Math.random()*squares.length);
		if (squares[randomnumber].innerHTML==0) {
			squares[randomnumber].innerHTML=2; 
		} else generate();
		for (let i=0; i<squares.length; i++) {
			if (parseInt(squares[i].innerHTML)==0) {
				squares[i].style.backgroundColor='yellow';
			}
			else if (parseInt(squares[i].innerHTML)==2) {
				squares[i].style.backgroundColor='#ffc266';
			}
			else if (parseInt(squares[i].innerHTML)==4) {
				squares[i].style.backgroundColor='#ff9900';
			}
			else if (parseInt(squares[i].innerHTML)==8) {
				squares[i].style.backgroundColor='#ff8c66';
			}
			else if (parseInt(squares[i].innerHTML)==16) {
				squares[i].style.backgroundColor='#ff6333';
			}
			else if (parseInt(squares[i].innerHTML)==32) {
				squares[i].style.backgroundColor='#cc3300';
			}
			else if (parseInt(squares[i].innerHTML)==64) {
				squares[i].style.backgroundColor=' #cc66ff';
			}
			else if (parseInt(squares[i].innerHTML)==128) {
				squares[i].style.backgroundColor=' #3333ff';
			}
			else if (parseInt(squares[i].innerHTML)==256) {
				squares[i].style.backgroundColor=' #3366ff';
			}
			else if (parseInt(squares[i].innerHTML)==512) {
				squares[i].style.backgroundColor=' #33ccff';
			}
			else if (parseInt(squares[i].innerHTML)==1024) {
				squares[i].style.backgroundColor='  #cccc00';
			}
			else if (parseInt(squares[i].innerHTML)==2048) {
				squares[i].style.background=' linear-gradient(to right, rgba(255,0,0,0), rgba(255,0,0,1))'
			}
		}
		losecheck();
	}

	//right
	function moveRight() {
		for(var i=0; i<16; i++) {
			if (i%4===0) {
				var totalOne=squares[i].innerHTML
				var totalTwo=squares[i+1].innerHTML
				var totalThree=squares[i+2].innerHTML
				let totalFour=squares[i+3].innerHTML
				let row=[parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour) ]
	

				var filteredRow=row.filter(num=>num);
		
				var missing= 4-filteredRow.length;
				var zeros=Array(missing).fill(0)
	
				let newRow=zeros.concat(filteredRow);
			
				
				squares[i].innerHTML=newRow[0];
				squares[i+1].innerHTML=newRow[1];
				squares[i+2].innerHTML=newRow[2];
				squares[i+3].innerHTML=newRow[3];		
			}
		}
	}

	function moveLeft() {
		for(var i=0; i<16; i++) {
			if (i%4===0) {
				var totalOne=squares[i].innerHTML
				var totalTwo=squares[i+1].innerHTML
				var totalThree=squares[i+2].innerHTML
				let totalFour=squares[i+3].innerHTML
				let row=[parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour) ]

				var filteredRow=row.filter(num=>num);
				var missing= 4-filteredRow.length;
				var zeros=Array(missing).fill(0)
	
				let newRow=filteredRow.concat(zeros);
		
				
				squares[i].innerHTML=newRow[0];
				squares[i+1].innerHTML=newRow[1];
				squares[i+2].innerHTML=newRow[2];
				squares[i+3].innerHTML=newRow[3];		
			}
		}
	}

	function moveDown() {
		for(let i=0; i<4; i++) {
			let totalOne=squares[i].innerHTML;
			let totalTwo=squares[i+width].innerHTML
			let totalThree=squares[i+width*2].innerHTML
			let totalFour=squares[i+width*3].innerHTML
			let column=[parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour) ];
			let filteredColumn=column.filter(num=>num);
			let missing=4-filteredColumn.length;
			let zeros=Array(missing).fill(0);
			let newColumn=zeros.concat(filteredColumn);

			squares[i].innerHTML=newColumn[0];
			squares[i+width].innerHTML=newColumn[1]
			squares[i+width*2].innerHTML=newColumn[2]
			squares[i+width*3].innerHTML=newColumn[3]
				

		}
	}

	function MoveUp() {
		for(let i=0; i<4; i++) {
			let totalOne=squares[i].innerHTML;
			let totalTwo=squares[i+width].innerHTML
			let totalThree=squares[i+width*2].innerHTML
			let totalFour=squares[i+width*3].innerHTML
			let column=[parseInt(totalOne), parseInt(totalTwo), parseInt(totalThree), parseInt(totalFour) ];
			let filteredColumn=column.filter(num=>num);
			let missing=4-filteredColumn.length;
			let zeros=Array(missing).fill(0);
			let newColumn=filteredColumn.concat(zeros);

			squares[i].innerHTML=newColumn[0];
			squares[i+width].innerHTML=newColumn[1]
			squares[i+width*2].innerHTML=newColumn[2]
			squares[i+width*3].innerHTML=newColumn[3]
				

		}
	}
	

	function combineRow() {
		for (var i=0; i<15; i++) {
			if (squares[i].innerHTML===squares[i+1].innerHTML) {
				let combinedTotal=parseInt(squares[i].innerHTML)+parseInt(squares[i+1].innerHTML)
				squares[i].innerHTML=combinedTotal;
				squares[i+1].innerHTML=0;
				score+=combinedTotal;
				scoreDisplay.innerHTML=score;
			}
		}
		wincheck();
		for (let i=0; i<squares.length; i++) {
			if (parseInt(squares[i].innerHTML)==0) {
				squares[i].style.backgroundColor='yellow';
			}
		}
	}

	function combineColumn() {
		for (var i=0; i<12; i++) {
			if (squares[i].innerHTML===squares[i+width].innerHTML) {
				let combinedTotal=parseInt(squares[i].innerHTML)+parseInt(squares[i+width].innerHTML)
				squares[i].innerHTML=combinedTotal;
				squares[i+width].innerHTML=0;
				score+=combinedTotal;
				scoreDisplay.innerHTML=score;
			}
		}
		wincheck();
		
	}

	function control(e) {
		if(e.key === 'ArrowRight') {
            keyRight();
        } else if (e.key==='ArrowLeft') {keyLeft()}
        else if (e.key==='ArrowUp') {keyUp()}
        	else if (e.key==="ArrowDown") {keyDown()}
	}

	document.addEventListener('keyup',control);

	function keyRight(){
		moveRight()
		combineRow()
		moveRight()
		generate()
	}

	function keyLeft(){
		moveLeft()
		combineRow()
		moveLeft()
		generate()

	}
	
	function keyDown() {
		moveDown();
		combineColumn();
		moveDown();
		generate();
	}

	function keyUp() {
		MoveUp();
		combineColumn();
		MoveUp();
		generate();

	}

	function wincheck(){
		for (let i=0; i<squares.length; i++) {
			if (squares[i].innerHTML=='2048') {resultDisplay.innerHTML='You have won';
			document.removeEventListener('keyup',control); togglepopup(); }
		}
		
		
		for (let i=0; i<squares.length; i++) {
			if (parseInt(squares[i].innerHTML)>1024) {
				squares[i].style.fontSize='400%';
			}
		}
	}

	function losecheck(){
		let zeros=0;
		for(let i=0; i<squares.length; i++) {
			if(squares[i].innerHTML==0) {
				zeros++;
			}
		}
		if (zeros===0) {
			resultDisplay.innerHTML='You lost';
			document.removeEventListener('keyup',control); togglepopup(); gridDisplay.style.opacity='0.5'; ;
		}

	}

	function togglepopup(){
    	let popup=document.getElementById('popup-1');
    	popup.classList.add('active');
    	let scoreshow=document.getElementById('scoreshow');
    	console.log('pop up should work');
    	

    }	






